import dataProcessingLibrary from "@/util/DataProcessing";

function assertEquals(actual, expected, testName) {
  const isEqual = JSON.stringify(actual) === JSON.stringify(expected);
  if (isEqual) {
    console.log(`Test passed: ${testName}`);
  } else {
    console.error(`Test failed: ${testName}\nExpected: ${JSON.stringify(expected)}\nActual: ${JSON.stringify(actual)}`);
  }
}

let dataProcessingTests = {

  testFilter: function () {
    // Test data
    const data = [
      {id: 1, name: 'Alice', age: 30},
      {id: 2, name: 'Bob', age: 25},
      {id: 3, name: 'Charlie', age: 22},
    ];

    // Filter criterion
    const ageAbove25 = function (item) {
      return item.age > 25;
    };

    // Expected result
    const expectedFilteredData = [
      {id: 1, name: 'Alice', age: 30},
    ];

    // Test filter function
    const filterResult = dataProcessingLibrary.filter(data, ageAbove25);
    assertEquals(filterResult, expectedFilteredData, 'Filter: age above 25');

  },
  testJoin: function () {
    const data1 = [
      {id: 1, name: 'Alice', age: 30},
      {id: 2, name: 'Bob', age: 25},
      {id: 3, name: 'Charlie', age: 22},
    ];

    const data2 = [
      {userId: 1, city: 'New York', country: 'USA'},
      {userId: 2, city: 'San Francisco', country: 'USA'},
      {userId: 4, city: 'Paris', country: 'France'},
    ];

    // Expected result
    const expectedJoinedData = [
      {id: 1, name: 'Alice', age: 30, userId: 1, city: 'New York', country: 'USA'},
      {id: 2, name: 'Bob', age: 25, userId: 2, city: 'San Francisco', country: 'USA'},
    ];

    // Test join function
    const joinResult = dataProcessingLibrary.join(data1, data2, 'id', 'userId');
    assertEquals(joinResult, expectedJoinedData, 'Join: inner join based on id and userId')
  },
  testTransform: function () {
    // Test data
    const data = [
      {id: 1, name: 'Alice', age: 30},
      {id: 2, name: 'Bob', age: 25},
      {id: 3, name: 'Charlie', age: 22},
    ];

    // Transformation function
    const extractAges = function (item) {
      return item.age;
    };

    // Expected result for transform
    const expectedTransformedData = [30, 25, 22];

    // Test transform function
    const transformResult = dataProcessingLibrary.transform(data, extractAges);
    assertEquals(transformResult, expectedTransformedData, 'Transform: extract ages');
  },
  testCount: function () {
    // Test data
    const data = [
      {id: 1, name: 'Alice', age: 30},
      {id: 2, name: 'Bob', age: 25},
      {id: 3, name: 'Charlie', age: 22},
    ];

    // Expected result for count
    const expectedCount = 3;

    // Test count function
    const countResult = dataProcessingLibrary.count(data);
    assertEquals(countResult, expectedCount, 'Count: number of items in the data array');
  },
  testCountIf: function () {
    // Test data
    const data = [
      {id: 1, name: 'Alice', age: 30},
      {id: 2, name: 'Bob', age: 25},
      {id: 3, name: 'Charlie', age: 22},
    ];

    // CountIf criterion
    const ageAbove25 = function (item) {
      return item.age > 25;
    };

    // Expected result for countIf
    const expectedCountIf = 1;

    // Test countIf function
    const countIfResult = dataProcessingLibrary.countIf(data, ageAbove25);
    assertEquals(countIfResult, expectedCountIf, 'CountIf: number of items with age above 25');
  },
  testSum: function () {
    // Test data
    const data = [1, 2, 3, 4, 5];

    // Expected result for sum
    const expectedSum = 15;

    // Test sum function
    const sumResult = dataProcessingLibrary.sum(data);
    assertEquals(sumResult, expectedSum, 'Sum: sum of the numbers in the array');
  },

  testAverage: function () {
    // Test data
    const data = [1, 2, 3, 4, 5];

    // Expected result for average
    const expectedAverage = 3;

    // Test average function
    const averageResult = dataProcessingLibrary.average(data);
    assertEquals(averageResult, expectedAverage, 'Average: average of the numbers in the array');
  },

  testOrder: function () {
    // Test data
    const data = [
      {id: 1, name: 'Alice', age: 30},
      {id: 2, name: 'Bob', age: 25},
      {id: 3, name: 'Charlie', age: 22},
    ];

    // Expected result for order
    const expectedOrderedData = [
      {id: 3, name: 'Charlie', age: 22},
      {id: 2, name: 'Bob', age: 25},
      {id: 1, name: 'Alice', age: 30},
    ];

    // Test order function
    const orderResult = dataProcessingLibrary.order(data, 'age', 'asc');
    assertEquals(orderResult, expectedOrderedData, 'Order: ascending order by age');
  },

  testUnique: function () {
    // Test data
    const data = [
      {id: 1, name: 'Alice', city: 'New York'},
      {id: 2, name: 'Bob', city: 'San Francisco'},
      {id: 3, name: 'Charlie', city: 'New York'},
      {id: 4, name: 'David', city: 'San Francisco'},
    ];

    // Expected result for unique
    const expectedUniqueData = [
      {id: 1, name: 'Alice', city: 'New York'},
      {id: 2, name: 'Bob', city: 'San Francisco'},
    ];

    // Test unique function
    const uniqueResult = dataProcessingLibrary.unique(data, 'city');
    assertEquals(uniqueResult, expectedUniqueData, 'Unique: unique city values');
  },

  testAppend: function () {
    // Test data
    const data1 = [
      {id: 1, name: 'Alice', age: 30},
      {id: 2, name: 'Bob', age: 25},
    ];

    const data2 = [
      {id: 3, name: 'Charlie', age: 22},
      {id: 4, name: 'David', age: 40},
    ];

    // Expected result for append
    const expectedAppendedData = [
      {id: 1, name: 'Alice', age: 30},
      {id: 2, name: 'Bob', age: 25},
      {id: 3, name: 'Charlie', age: 22},
      {id: 4, name: 'David', age: 40},
    ];

    // Test append function
    const appendResult = dataProcessingLibrary.append(data1, data2);
    assertEquals(appendResult, expectedAppendedData, 'Append: append two data arrays');
  },

  testUpsert: function () {
    // Test data
    const data1 = [
      {id: 1, name: 'Alice', age: 30},
      {id: 2, name: 'Bob', age: 25},
    ];

    const data2 = [
      {id: 2, name: 'Bob', age: 26},
      {id: 3, name: 'Charlie', age: 22},
    ];

    // Expected result for upsert
    const expectedUpsertData = [
      {id: 1, name: 'Alice', age: 30},
      {id: 2, name: 'Bob', age: 26},
      {id: 3, name: 'Charlie', age: 22},
    ];

    // Test upsert function
    const upsertResult = dataProcessingLibrary.upsert(data1, data2, 'id');
    assertEquals(upsertResult, expectedUpsertData, 'Upsert: upsert data2 into data1 based on id');
  },

  testSplit: function () {
    // Test data
    const data = [
      {id: 1, name: 'Alice', age: 30},
      {id: 2, name: 'Bob', age: 25},
      {id: 3, name: 'Charlie', age: 22},
    ];

    // Split criterion
    const ageAbove25 = function (item) {
      return item.age > 25;
    };

    // Expected result for split
    const expectedSplitData = {
      group1: [
        {id: 1, name: 'Alice', age: 30},
      ],
      group2: [
        {id: 2, name: 'Bob', age: 25},
        {id: 3, name: 'Charlie', age: 22},
      ],
    };

    // Test split function
    const splitResult = dataProcessingLibrary.split(data, ageAbove25);
    assertEquals(splitResult, expectedSplitData, 'Split: split data into two groups based on age above 25');
  },
  runTests: function () {
    this.testFilter();
    this.testJoin();
    this.testTransform();
    this.testCount();
    this.testCountIf();
    this.testSum();
    this.testAverage();
    this.testOrder();
    this.testUnique();
    this.testAppend();
    this.testUpsert();
    this.testSplit();
  }
}

export default dataProcessingTests