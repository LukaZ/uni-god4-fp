import axios from "axios";
import {createObjectCsvWriter} from "csv-writer";

let dataProcessingLibrary = {
  filter: function (data, criterion) {
    if (typeof criterion !== 'function') {
      throw new Error('Criterion must be a function');
    }

    let filteredData = [];
    for (let i = 0; i < data.length; i++) {
      if (criterion(data[i])) {
        filteredData.push(data[i]);
      }
    }
    return filteredData;
  },

  transform: function (data, transformation) {
    if (typeof transformation !== 'function') {
      throw new Error('Transformation must be a function');
    }

    let transformedData = [];
    for (let i = 0; i < data.length; i++) {
      transformedData.push(transformation(data[i]));
    }
    return transformedData;
  },

  count: function (data) {
    return data.length;
  },

  countIf: function (data, criterion) {
    if (typeof criterion !== 'function') {
      throw new Error('criterion must be a function');
    }
    let filteredData = this.filter(data, criterion);
    return filteredData.length;
  },

  sum: function (data) {
    let sum = 0;
    for (let i = 0; i < data.length; i++) {
      sum += data[i];
    }
    return sum;
  },

  average: function (data) {
    let sum = this.sum(data);
    return sum / data.length;
  },

  order: function (data, key, order, comparator) {
    if (order === undefined) {
      order = 'asc';
    }

    let sortedData = data.slice(0);

    if (!comparator) {
      comparator = function (a, b) {
        if (a[key] < b[key]) return order === 'asc' ? -1 : 1;
        if (a[key] > b[key]) return order === 'asc' ? 1 : -1;
        return 0;
      };
    }

    for (let i = 0; i < sortedData.length; i++) {
      for (let j = i + 1; j < sortedData.length; j++) {
        if (comparator(sortedData[i], sortedData[j]) > 0) {
          let temp = sortedData[i];
          sortedData[i] = sortedData[j];
          sortedData[j] = temp;
        }
      }
    }

    return sortedData;
  },

  unique: function (data, key) {
    let uniqueData = [];
    let seen = [];

    for (let i = 0; i < data.length; i++) {
      let item = data[i];
      let itemKey = item[key];
      let alreadySeen = false;

      for (let j = 0; j < seen.length; j++) {
        if (seen[j] === itemKey) {
          alreadySeen = true;
          break;
        }
      }

      if (!alreadySeen) {
        seen.push(itemKey);
        uniqueData.push(item);
      }
    }

    return uniqueData;
  },

  join: function (data1, data2, key1, key2) {
    if (key2 === undefined) {
      key2 = key1;
    }

    let joinedData = [];

    for (let i = 0; i < data1.length; i++) {
      let item1 = data1[i];

      for (let j = 0; j < data2.length; j++) {
        let item2 = data2[j];

        if (item1[key1] === item2[key2]) {
          let joinedItem = {};
          for (let key in item1) {
            joinedItem[key] = item1[key];
          }
          for (let key in item2) {
            joinedItem[key] = item2[key];
          }
          joinedData.push(joinedItem);
        }
      }
    }

    return joinedData;
  },

  append: function (data1, data2) {
    let appendedData = [];


    for (let i = 0; i < data1.length; i++) {
      appendedData.push(data1[i]);
    }

    for (let i = 0; i < data2.length; i++) {
      appendedData.push(data2[i]);
    }

    return appendedData;
  },

  upsert: function (data1, data2, key1, key2) {
    if (key2 === undefined) {
      key2 = key1;
    }


    let newData = this.append(data1, []);

    for (let i = 0; i < data2.length; i++) {
      let item2 = data2[i];
      let index = -1;

      for (let j = 0; j < newData.length; j++) {
        if (newData[j][key1] === item2[key2]) {
          index = j;
          break;
        }
      }

      if (index === -1) {
        newData.push(item2);
      } else {
        for (let key in item2) {
          newData[index][key] = item2[key];
        }
      }
    }

    return newData;
  },

  split: function (data, criterion) {
    if (typeof criterion !== 'function') {
      throw new Error('criterion must be a function');
    }

    let splitData = {
      group1: [],
      group2: [],
    };

    for (let i = 0; i < data.length; i++) {
      let item = data[i];

      if (criterion(item)) {
        splitData.group1.push(item);
      } else {
        splitData.group2.push(item);
      }
    }

    return splitData;
  },

  readCSV: async function (path) {
    try {
      const response = await axios.get(path, {responseType: 'text'});
      return response.data;
    } catch (error) {
      console.error(error);
    }
  },

  readJSON: async function (path) {
    try {
      const response = await axios.get(path, {responseType: 'json'});
      return response.data;
    } catch (error) {
      console.error(error);
    }
  },


  async writeCSV(data, path) {
    // Determine headers from data
    let headers = Object.keys(data[0]).map((key) => ({ id: key, title: key }));

    let csvWriter = createObjectCsvWriter({
      path: path,
      header: headers,
    });

    try {
      await csvWriter.writeRecords(data);
      console.log(`Successfully wrote CSV to ${path}`);
    } catch (error) {
      console.error(`Error writing CSV to ${path}:`, error);
    }
  },

  writeJSON: function (data, path) {
    let jsonData = JSON.stringify(data);
    let blob = new Blob([jsonData], {type: 'application/json;charset=utf-8;'});
    saveAs(blob, path);
  },

  plotSVG: function (data, path, handler) {
    // Ran out of time :/
  },
};

export default dataProcessingLibrary