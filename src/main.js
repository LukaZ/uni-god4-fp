import { createApp } from 'vue'
import App from './App.vue'

import './assets/main.css'
import 'primevue/resources/themes/lara-dark-blue/theme.css'
import 'primevue/resources/primevue.min.css'
import 'primeicons/primeicons.css'
import 'primeflex/primeflex.css'

import PrimeVue from 'primevue/config'

let app = createApp(App)
app.use(PrimeVue, { ripple: true })
app.mount('#app')